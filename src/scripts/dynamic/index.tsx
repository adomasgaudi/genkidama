import React, { Component } from "react"

import { createElement } from "react"

const dynamic = (component: any) => (baseTexts: any, ...linkedTexts: any) => class extends Component {
  render(){
    
    let items: any = []
    for (let i = 0; i < baseTexts.length; i++) {
      // console.log({i});
      
      // console.log([linkedTexts[i]]);
      items = [...items,(
        <>
          <span>{baseTexts[i]}</span>
          {linkedTexts[i] ? createElement(component, {object: linkedTexts[i][1]}, linkedTexts[i][0]) : null}
        </> 
      )]
    }

    return (
      <>
        {items}
      </>
    )
  }
}



const dynamicTest = (component: any) => (baseTexts: any, ...linkedTexts: any) => class extends Component {
  render(){
    
    let items: any = []
    for (let i = 0; i < baseTexts.length; i++) {
      // console.log({i});
      
      // console.log([linkedTexts[i]]);
      items = [...items,(
        <>
          <span>{baseTexts[i]}</span>
          {linkedTexts[i] ? createElement(component, {object: linkedTexts[i][1]}, linkedTexts[i][0]) : null}
        </> 
      )]
    }

    return (
      <>
        {items}
      </>
    )
  }
}



export {dynamic, dynamicTest}