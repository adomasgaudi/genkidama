import { BaseCSS, FunkyCSS, GeneralCSS, themeFunction } from 'assets'
import { Book, Main } from 'domains'

import { Route } from 'react-router'
import { Routes } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro';

const CssComp = () => (<><BaseCSS /><FunkyCSS /><GeneralCSS /></>)

function App() {
  return (
    <>
      <ThemeProvider theme={ themeFunction(0, 0) }>
        <CssComp />

        <div className='App'>
          <Routes>
            <Route path='/' element={<Main />} />
            <Route path='/book' element={<Book />} />
          </Routes> 
        </div>
      </ThemeProvider>
    </>
  )
}

export default App
