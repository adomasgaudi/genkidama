import Container from "@mui/material/Container"
import styled from 'styled-components';

const MyContainer = styled(Container)`
padding: 2rem .3rem 1rem .3rem;
@media (min-width: 960px) {
  /* background: red!important; */
  max-width: 750px !important;
}
.MuiPaper-root {
}
`

export {MyContainer}