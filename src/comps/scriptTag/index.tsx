import React, { useState } from 'react'

import styled from 'styled-components'

const ColorDiv = styled.span<{colorON: any}>`
  color: ${({colorON}) => colorON ? 'red' : 'black'};
  text-decoration: underline;
  text-decoration-thickness: 2px;
  text-decoration-color: rgba(0, 0, 0, 0.1); 
  &:hover{
    text-decoration: underline;
    text-decoration-thickness: 2px;
    text-decoration-color: #dd2c2c;
  }
`


const ScriptTag: React.FC<{object: any}> = ({children, object}) => {

  // console.log('script inside');
  console.log({object});

  const [final, setFinal] = useState(children);
  const [color, setColor] = useState(false)
  
  const clickHandler = () => {
    // alert(object.text)
    if(object.alt)
    final === object.alt ? setFinal(children) : setFinal(object.alt[0])
    console.log({final});
    color === false ? setColor(true) : setColor(false)
  } 

  // console.log(final);
  
  return (
    <ColorDiv colorON={color} onClick={clickHandler} >
      {final}
    </ColorDiv>
  )
}

export default ScriptTag
