import {BaseCSS, FunkyCSS, GeneralCSS} from './styles/styles'

import { themeFunction, } from './styles/theme'

export { themeFunction, BaseCSS, FunkyCSS, GeneralCSS}