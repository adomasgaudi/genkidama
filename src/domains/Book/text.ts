import { essentialismArticle } from "./essentialismArticle"

class Book {
  short: string
  constructor(short: string) {
    this.short = short;
  }
  get shorts() {
    return this.short
  }
  shortNum() {
    return this.short.length
  }
}




const fiveSecondRule = {
  howTo: `5 Second rule gives enormous will power. Based on the 5 second rule commitment. Rests all will power on the willpower of a single habit.`,
  dfs: `willpower  ` ,
  book: new Book(`5 Second rule gives enormous will power. Based on the 5 second rule commitment. Rests all will power on the willpower of a single habit.`)
}

const habits = `why you should create habits`

// const mainText = `${fiveSecondRule.howTo}`;


const ideas =  `
  to be productive? 
`



const text1 = `${ideas}`
console.log(ideas);



const list = [
  `essentialism / deep work /thinking / booredom`, 
  `5 second rule`, 
  `progressive extreemism`, 
  `growth midset`,
  `learning to learn`,
  `interpolation / chunking / systems`,
  `the power of habit`
]

const happiness = 'do things that give you dopamine'

const howtolive = () => {
  return `Philosophies on ${happiness} to live: achieve maximum happiness and fulfilment`
}




class Pillar {
  value: number
  name: string
  constituents: string
  constructor(value: number, name: string, constituents:string) {
    this.value = value;
    this.name = name;
    this.constituents = constituents;
  }
}



let habit = new Pillar(1, "habit", 'none')

let efficiency = new Pillar(1,'efficiency', 'essentialism + compound effect + learning')

let result = new Pillar(1, 'result', `${habit.name} + willpower + identity`)




let productivity = {
  value: 1,
  name: 'productivity',
  constituents: `((${result.constituents}) * (${efficiency.constituents})) over time`
}

let time = {
  value: 100
}

productivity.value = result.value / time.value

let text2 = `${productivity.name} is the ${productivity.constituents}.`
// let text3 = `${productivity.name} is the ${productivity.constituents}.`







const mainText = [essentialismArticle.edited, text2]





export {mainText}